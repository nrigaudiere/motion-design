/**
 * @author Nicolas
 */
var controller = new Leap.Controller({enableGestures : true});
target = 0
//Shows Leap status
controller.on('connect', function() {
  console.log("Successfully connected.");
});

controller.on('deviceConnected', function() {
  console.log("A Leap device has been connected.");
});

controller.on('deviceDisconnected', function() {
  console.log("A Leap device has been disconnected.");
});

controller.on('gesture', function (gesture){
    //console.log(gesture);
    if(gesture.type === 'swipe'){
        handleSwipe(gesture);
    }
    
    if(gesture.type === 'keyTap' || gesture.type === 'screenTap'){
    	onclickActiveItem(cf.getActiveItem());
    }
});

function handleSwipe (swipe){
    var startFrameID;
    if(swipe.state === 'stop'){
        if (swipe.direction[0] > 0){
            //this means that the swipe is to the right direction
            
            console.log("Swipe Right");
            
            cf.moveTo('pre');
            
            //moveRight();
        }else{
            //this means that the swipe is to the left direction
            
            console.log("Swipe Left");
            cf.moveTo('next');
            
            //moveLeft();
        }
    }
}

function onclickActiveItem (item) {
    var url
    target = item.content.getAttribute('id')
    url = "video/" + target
    window.location.href = url 
}

function endVideo(id){
    target = id
    window.location.href = '/'+ target 
}

function init(id) {
    cf = new ContentFlow('contentFlow', {reflectionColor: "#000000", circularFlow: false, startItem : id});
    target = id
}

controller.connect();